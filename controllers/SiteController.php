<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\modules\blog\models\Post;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use app\models\MaterialSearch;
use app\models\Material;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'actions' => ['index', 'about'],
                        'roles' => ['?'],
                    ],

                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'about',],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {   $searchModel = new MaterialSearch();
        $limit = 6;
        $params =[];
        if (
            !empty($_GET) && 
            isset($_GET['page_count']) && 
            $_GET['page_count']
        )   {
            $params['page_count'] =  (int) $_GET['page_count'];
        }
        $id= Yii::$app->user->id;
        $params['favorit'] = $id;
        if (
            !empty($_GET) && 
            isset($_GET['city_id']) && 
            $_GET['city_id']
        )   {
            $params['city_id'] = $_GET['city_id'];    
        };
        if (!isset($_GET['page_count'])) {
            $params['page_count'] = 1;
        }
        $result = $searchModel->asyncSearch($params);
        if (!isset($result['result'])) {
            $result['result'] = [];
            $result['pages'] = [];
        }
        return $this->render('/material/index',[
            'result' => $result['result'],
            'pages'=>$result['pages'],
            'favorit'=> $params['favorit']
        ]);    
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if (
            $model->load(Yii::$app->request->post()) && 
            $model->contact(Yii::$app->params['adminEmail'])
        ) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
}