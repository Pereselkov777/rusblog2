<?php

namespace app\controllers;

use Yii;
use app\models\Material;
use app\models\MaterialSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\filters\AccessControl;
use app\models\Photo;
use app\models\City;
use app\models\NewsCity;

/**
 * MaterialController implements the CRUD actions for Material model.
 */
class MaterialController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'ajax', 'delete', 'ajax2','upload','favorites'],
                'rules' => [
                    [
                        'actions' => ['index','create', 'update', 'ajax', 'delete','ajax2','upload','favorites'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {            
        if ($action->id == 'create' ||$action->id == 'upload') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all Material models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MaterialSearch();
        $limit = 6;    
        $params =[];
        if (
            !empty($_GET) &&
            isset($_GET['page_count']) &&
            $_GET['page_count']
        )   {
            $params['page_count'] =  (int)$_GET['page_count'];
        }
        if ( 
            !empty($_GET) && 
            isset($_GET['city_id']) 
            && $_GET['city_id']
        ) {
            $params['city_id'] = $_GET['city_id'];
        };
         //Base Search
        if (!isset($_GET['page_count'])) {
            $params['page_count'] = 1;
        }
        $result = $searchModel->asyncSearch($params);
        if (!isset($result['result'])) {
            $result['result'] = [];
            $result['pages'] = [];
        }
        return $this->render('index', [
            "result" =>$result['result'],
            "pages"=>$result['pages'],
            'favorit'  => null,
            'filter_id'=>null,
        ]);
    }
   
    public function actionAjax()
    { 
        //Search 
        if( 
            !empty($_GET) && 
            isset($_GET['val']) && 
            $_GET['val']
        )   {
                $params = [];
                if(isset($_GET['page_count'])) {
                    $params['page_count'] = Html::encode($_GET['page_count']);
                }
                $searchModel = new MaterialSearch();
                $params['val'] = $_GET['val'];
                $result = $searchModel->asyncSearch($params);
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if (!isset($result['result'])) {
                    $result['result'] = [];
                    $result['pages'] = [];
                    $result['val'] = [];
                }
                $results = [
                    'val' =>$result['val'],
                    'paginated_search'=>$result['pages'],
                    'result' => $result['result'],
                    
                ];
                //var_dump($results); exit();
                return $results;  
        }   else   {
            return $this->redirect(['index']);

        }
    }
    /**
     * Creates a new Material model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {    
        $model = new Material();
        if  (Yii::$app->request->post() && Yii::$app->request->isAjax) {
            $_POST['title'] = Html::encode($_POST['title']);
            $_POST['description'] = Html::encode($_POST['description']);
            $_POST['text'] = Html::encode($_POST['text']);
            $model->title = $_POST['title'];
            $model->description = $_POST['description'];
            $model->text = $_POST['text'];
            $model->city_id = $_POST['city'];
            if  (!$model->validate()) {
                \Yii::$app->getSession()->setFlash('error', 'Некорректный ввод');
                print_r(json_encode($model->getErrors()));
                exit();
            }
            if ($model->save()) {
                if(!Material:: getFields(Yii::$app->user->id)){
                    Material::createDefaultFields(Yii::$app->user->id);            
                }
                Yii::$app->db->createCommand()->insert('news_city', [
                'news_id' => intval($model->id),
                'city_id' => intval($_POST['city']),
                ])->execute();
                \Yii::$app->getSession()->setFlash('success', 'Сохранено');  
                $result=[];
                $result['id']=$model->id;
                print_r(json_encode($result));
                exit();
            }
        }
        if (Yii::$app->request->post() && !Yii::$app->request->isAjax) {
            $model->load(Yii::$app->request->post());
        }
        return $this->render('create', [
            'model' => $model, 'update'=>true
        ]);
       
    }
    public function actionFavorites($id)
    {
        if ($id) {
            Yii::$app->db->createCommand()->insert('news_users', [
                'news_id' => intval($id),
                'user_id' => intval(Yii::$app->user->id),
            ])->execute();
            return $this->redirect('/material/index');
        }
    }
    public function actionUpload()
    { 
        if (Yii::$app->request->post()) {
            if (isset($_FILES['photo']) && $_POST['id']) {
                $filename = $_FILES['photo']['name'];
                if (!isset($_FILES['photo']['type'])) {
                    echo('NO TYPE');
                    echo 'fatal error: no support type'; exit;

                }
                $mime_type = $_FILES['photo']['type'];
                $allowed_file_types = ['image/png', 'image/jpeg'];
                if (!in_array($mime_type, $allowed_file_types)) {
                    echo 'Not allowed type '. $mime_type;
                     exit;
                }
                $e = explode('.', $filename);
                $ext = end($e);
                $name = time() . '.' . $ext;
                $material = Material::findOne($_POST['id']);
                if (isset($_POST['is_new_photo'])) {
                    $model= new Photo();
                } else { 
                    $model = Photo::findOne($material->photo_id);
                }
                if ($material->photo_id == null) {
                    $model= new Photo();
                }
                $model->name = $name;  
                $uploadDirFull = '/uploads/full/';
                $path = (Yii::getAlias('@webroot') . $uploadDirFull . $name);
                try {
                    if (is_uploaded_file ($_FILES ['photo'] ['tmp_name'])) {
                            move_uploaded_file ($_FILES ['photo'] ['tmp_name'],
                            Yii::getAlias('@webroot') . $uploadDirFull . $name);
                    };
                } catch (Exception $e) {
                    echo'Error:', $e->getMessage (). PHP_EOL;
                };
                if ($ext == 'png') {
                    $image = imagecreatefrompng(Yii::getAlias('@webroot') . '/uploads/full/' . $name);
                    $im = imagecreatefrompng(Yii::getAlias('@webroot') . '/uploads/full/' . $name);
                    $im2 = imagecrop($im, ['x' => 0, 'y' => 0, 'width' => 50, 'height' => 70]);
                    $im2 = imagescale($im, 50, 70);
                    if ($im2 !== FALSE) {
                        imagepng($im2, Yii::getAlias('@webroot') . '/uploads/thumbs/' . $name);
                        imagedestroy($im2);
                    }
                    imagedestroy($im);
                } else {    
                    if (file_exists(Yii::getAlias('@webroot') . '/uploads/full/' . $name)) {
                        $image = imagecreatefromjpeg(Yii::getAlias('@webroot') . '/uploads/full/' . $name);
                        $im = imagecreatefromjpeg(Yii::getAlias('@webroot') . '/uploads/full/' . $name);
                        $im2 = imagecrop($im, ['x' => 0, 'y' => 0, 'width' => 50, 'height' => 70]);
                        $im2 = imagescale($im, 50, 70);
                        if ($im2 !== FALSE) {
                            imagejpeg($im2, Yii::getAlias('@webroot') . '/uploads/thumbs/' . $name);
                            imagedestroy($im2);
                        }
                        imagedestroy($im);     
                    } else { 
                    echo('Error');
                    exit();
                    }  
                }
                $model->full = $model->thumbs=$name;
                if (!$model->save()) {
                    echo('STOP');
                    var_dump($model->getErrors());
                };
                if ($material) {
                    $material->photo_id = $model->id;
                    if (!$material->save()) {
                        var_dump($material->getErrors());exit();
                    };
                }
                return $this->redirect(['index']);    
            }

        }
        
    }
    /**
     * Updates an existing Material model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionAjax2()
    {
        if (
            !empty($_POST) && 
            isset($_POST['id']) && 
            $_POST['id']
            ) {
            if ( Yii::$app->request->post() && 
                Yii::$app->request->isAjax ) {
                $model = $this->findModel($_POST['id']);
                $_POST['title'] = Html::encode($_POST['title']);
                $_POST['description'] = Html::encode($_POST['description']);
                $_POST['text'] = Html::encode($_POST['text']);
                $_POST['city_id'] =(int)$_POST['city'];
                if ($_POST['city_id']) {
                    $rel = NewsCity::find()->where(['news_id'=>$_POST['id']])->one();
                    $rel->city_id = $_POST['city_id'];
                    $rel->save();
                }
                $model->title = $_POST['title'];
                $model->description = $_POST['description'];
                $model->text = $_POST['text'];
                $model->city_id = $_POST['city_id'];
                if (!$model->validate()) {        
                    print_r(json_encode($model->getErrors()));
                    exit();
                }
                if ($model->save()) {
                   print_r(json_encode('success'));
                   exit();
                }
            }            
        };    
    }

    /**
     * Deletes an existing Material model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete()
    {   
        if (!empty($_GET)) {
            $user_id = Yii::$app->user->id;
           foreach ($_GET as $p) {
              foreach ($p as $sub) {

                $this->findModel((int)$sub)->delete();
                $rels = NewsCity::find()
                ->where(['news_id'=>intval($sub)])
                ->one()
                ->delete();
              }
           } 
           $post['status'] = 'success';
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'result' => 'success',
                'status_code'=>200,
                
            ];
        }
       die('fail');
    }

    /**
     * Finds the Material model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Material the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Material::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}