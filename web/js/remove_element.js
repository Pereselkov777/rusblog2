$(".data").on("click", function (e) {
  elem = $(this).closest(".tr");
  var id = $(this).data("id");
  var html =
    "<button data-id=" +
    id +
    ' type="button" class="btn btn-primary del">Удалить</button>';
  if (id) {
    $("#add").empty();
    $("#add").append(html);
  }
  $(".del").bind("click", function () {
    $.ajax({
      url: "/material/delete",
      method: "POST",
      data: {
        id: id,
      },
      dataType: "json",
      success: function (data) {
        elem.remove();
        $(".modal").hide();
        window.location.reload(false);
      },
    });
  });
  e.preventDefault();
});
