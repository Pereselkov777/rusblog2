$(".data").on("click", function (e) {
  elem = $(this).closest(".li");
  var id = $(this).data("id");
  var html =
    "<button data-id=" +
    id +
    ' type="button" class="btn btn-primary del">Удалить</button>';
  if (id) {
    $("#add").empty();
    $("#add").append(html);
  }
  $(".del").bind("click", function () {
    $.ajax({
      url: "/type/delete",
      method: "POST",
      data: {
        id: id,
      },
      dataType: "json",
      success: function (data) {
        console.log(data, "success");
        console.log(elem);
        elem.remove();
        $(".modal").hide();
        window.location.reload(false);
      },
    });
  });
  e.preventDefault();
});
