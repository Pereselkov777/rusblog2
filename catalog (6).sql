-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 23 2021 г., 09:44
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `catalog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Название'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `name`) VALUES
(1, 'Москва'),
(2, 'Новосибирск');

-- --------------------------------------------------------

--
-- Структура таблицы `field`
--

CREATE TABLE `field` (
  `id` int(10) NOT NULL COMMENT 'ID',
  `description` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'sku',
  `title` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'title',
  `text` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'quantity',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'type',
  `client_id` int(10) NOT NULL,
  `photo` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `field`
--

INSERT INTO `field` (`id`, `description`, `title`, `text`, `type`, `client_id`, `photo`) VALUES
(8, 1, 1, 0, 0, 5, 1),
(9, 1, 1, 1, 1, 6, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `material`
--

CREATE TABLE `material` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID материала',
  `city_id` tinyint(4) UNSIGNED NOT NULL COMMENT 'Город',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Название',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'description',
  `photo_id` int(11) DEFAULT NULL COMMENT 'Изображение',
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Text'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `my_user`
--

CREATE TABLE `my_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `email_confirm_token` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 0,
  `role` smallint(6) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `my_user`
--

INSERT INTO `my_user` (`id`, `created_at`, `updated_at`, `username`, `auth_key`, `email_confirm_token`, `password_hash`, `password_reset_token`, `email`, `status`, `role`) VALUES
(5, 1625060241, 1625060241, 'mytestname', 'gNzRqCjp5oc5LAqkvI5IXccv2n0e5avG', NULL, '$2y$13$TZZkirDOu4AFX6Coc2kJzubzeY7QUvI7hmCksewDsrLp3OoJBC/Ca', NULL, 'aleksandrpereselkov77@gmail.com', 1, 1),
(6, 1626833651, 1626833651, 'testname2', '1dilBeY0Pkw6riF-5ef_dbGe5QmcK873', 'P5wBKqAF2psHMsLNSTmhxSkb_ctCWfcH', '$2y$13$4BjyFRBfOZjnlMZFmiaocOTnm1.FScA8zZdBSaK5TvPDin8vhpNEO', NULL, 'ruslan.peresy@yandex.ru', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `news_city`
--

CREATE TABLE `news_city` (
  `news_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `news_users`
--

CREATE TABLE `news_users` (
  `news_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `photo`
--

CREATE TABLE `photo` (
  `id` int(10) NOT NULL COMMENT 'ID изображения',
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Название',
  `full` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'full',
  `thumbs` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'thumbs'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `field`
--
ALTER TABLE `field`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`);

--
-- Индексы таблицы `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`city_id`),
  ADD KEY `SKU` (`description`),
  ADD KEY `title` (`title`);

--
-- Индексы таблицы `my_user`
--
ALTER TABLE `my_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx-user-username` (`username`),
  ADD UNIQUE KEY `idx-user-email` (`email`);

--
-- Индексы таблицы `news_city`
--
ALTER TABLE `news_city`
  ADD PRIMARY KEY (`news_id`,`city_id`),
  ADD KEY `city_id` (`city_id`);

--
-- Индексы таблицы `news_users`
--
ALTER TABLE `news_users`
  ADD PRIMARY KEY (`news_id`,`user_id`),
  ADD KEY `pkey` (`user_id`);

--
-- Индексы таблицы `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `full` (`full`),
  ADD KEY `thumbs` (`thumbs`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `field`
--
ALTER TABLE `field`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `material`
--
ALTER TABLE `material`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID материала', AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT для таблицы `my_user`
--
ALTER TABLE `my_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID изображения';

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `news_users`
--
ALTER TABLE `news_users`
  ADD CONSTRAINT `news_users_ibfk_1` FOREIGN KEY (`news_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `news_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `my_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
