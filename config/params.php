<?php

return [
    'adminEmail' => getenv('APP_MAILER_USERNAME'),
    'adminPassword' => getenv('APP_MAILER_PASSWORD'),
    'contactEmail' => getenv('APP_MAIL_CONTACT'),
    'supportEmail' => getenv('APP_MAILER_USERNAME'),
    'senderEmail' => getenv('APP_MAILER_USERNAME'),
    'senderName' => 'Info',
    'domain' => getenv('APP_URL'),
    'chat_host' => getenv('CHAT_HOST'),
    'user.passwordResetTokenExpire' => 3600,
];