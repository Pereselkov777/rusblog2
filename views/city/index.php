<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CityeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cities');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/remove_category.js?t=' . time(), ['depends' => [yii\web\JqueryAsset::className()]]);
?>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary cancel" data-bs-dismiss="modal">Отмена</button>
                <div id="add"></div>
            </div>
        </div>
    </div>
</div>
<h1 class="my-md-5 my-4">Типы товара</h1>
<a class="btn btn-primary mb-4" href="<?php echo Url::to(['/city/create'], true);?>" role="button">Добавить</a>
<div class="row">
    <div class="col-md-6">
        <ul class="list-group mb-4">
            <li class="list-group-item d-flex justify-content-between">
                <strong>Название</strong>
            </li>
            <?php echo ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '_single',
                        'layout' => "{items}{pager}",
                        'pager' => [
                        'firstPageLabel' => 'first',
                        'lastPageLabel' => 'last',
                        'nextPageLabel' => 'next',
                        'prevPageLabel' => 'previous',
                        'maxButtonCount' => 3,
                        ],
            ]);?>


        </ul>
    </div>