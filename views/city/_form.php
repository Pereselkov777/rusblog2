<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if (isset($_GET['id'])) {
    $this->title = Yii::t('app', 'Редактировать');
} else {
    $this->title = Yii::t('app', 'Добавить категорию');
}

/* @var $this yii\web\View */
/* @var $model app\models\Cities */
/* @var $form yii\widgets\ActiveForm */
?>

<h1 class="my-md-5 my-4"><?= Html::encode($this->title) ?></h1>
<div class="row">
    <div class="col-lg-5 col-md-8">
        <?php if (isset($_GET['id'])): ?>
        <form class="parent">
            <div class="form-floating mb-3">
                <input name="name" type="text" class="form-control name" placeholder="Напишите название"
                    id="floatingName">
                <label id="label_name" for="floatingName">
                    <?= !empty($model->name) ? $model->name : 'Название'?></label>
                <div class="invalid-feedback name_error">
                    Пожалуйста, выберите корректное значение
                </div>
            </div>

            <button data-id="<?= $model->id?>" id="update" class="btn btn-primary">Добавить</button>

        </form>
        <?php endif; ?>
        <?php if (!isset($_GET['id'])): ?>
        <form class="parent">
            <div class="form-floating mb-3">
                <input name="name" type="text" class="form-control name" placeholder="Напишите название"
                    id="floatingName">
                <label id="label_name" for="floatingName">
                    <?= !empty($model->name) ? $model->name : 'Название'?></label>
                <div class="invalid-feedback name_error">
                    Пожалуйста, выберите корректное значение
                </div>
            </div>


            <button id="create" class="btn btn-primary">Добавить</button>

        </form>
        <?php endif; ?>

    </div>
</div>

<?php
$script = <<< JS
    jQuery(document).ready(function () {
        var elem;
        var url;
        data = {

        };
        if($("#update").length){
                var elem = $("#update");
                var url = "/city/ajax";
                data.id = elem.data("id");

                actionCity(elem,url,data);
                return;
        }
        if($("#create").length){
            console.log($("#create"), "CREATE");
                var elem = $("#create");
                var url = "/city/create";
                actionCity(elem,url,data);
                return;
        }
        
        
        function actionCity(elem,url,data){
            elem.click(function(e) {
            var input = elem.closest('.parent').find('.name');
            var name = elem.closest('.parent').find('.name').val();
            data.name = name;
           $.ajax({
                url: url,
                method: "POST",
                data: data,
                dataType: "json",
                success: function (data) {
                    if(data.name){
                        $('.name_error').show();
                        input.val('');
                        alert("Некорректный ввод");
                        return;
                    }
                    input.val(name);
                    window.location.href = "index";
                    
                },

            }); 
            e.preventDefault();
        }); 

        }

              
    });

JS;
$this->registerJs($script);
?>