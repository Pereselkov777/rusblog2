<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use anmaslov\autocomplete\AutoComplete;
use yii\web\JsExpression;
use app\models\Cities;
use app\models\Material;


/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */  
$this->title = Yii::t('app', 'Materials');
$this->params['breadcrumbs'][] = $this->title;
$models = Cities::getCityList();
if($favorit){
    $filter_id =$favorit;
    $this->title = Yii::t('app', 'Избранные новости');
} else {
    $favorit = null;
    $this->title = Yii::t('app', 'Новости');
}

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/remove_element.js?t=' . time());

?>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
    crossorigin="anonymous"></script>
<script type="text/javascript">
$(document).ready(function() {
    var fiter_id = "<?php echo $filter_id;?>";
    if (fiter_id) {
        $("#productType").change(function(e) {
            window.location.href = "<?php echo Url::to(['/site/index/?city_id='], true);?>" + $(
                    this)
                .val() + "&favorit=true";
            return false;

        });
    } else {
        $("#productType").change(function(e) {
            window.location.href = "<?php echo Url::to(['/material/index/?city_id='], true);?>" + $(
                this).val();
        });
    }

    /*$("#my_input").on("change paste keyup", function(e) {
        rerenderList(e);
    });*/
    $("#my_input").on("change", function(e) {
        rerenderList(e);
    });
    $(".find").on("click", function(e) {
        rerenderList(e);
    });
    $(".page-item").on("click", function() {
        var page_count = $(this).data("count");
    });
    $(".form-check-input").on("change", function(e) {
        if ($(".modal").hasClass("show")) {
            if (
                $(this).hasClass('check')
            ) {
                $(this).removeClass('check');
                $(this).val('0');
                return false;
            }
            $(this).addClass('check');
            $(this).val($(this).data('name'));
            e.preventDefault();
        }
    });
    var items = [];
    $(".items").on("change", function(e) {

        if (
            $(this).hasClass('delitable')
        ) {
            $(this).removeClass('delitable');
            $(this).val('');
            position = $.inArray($(this).data('id'), items);
            if (~position) {
                items.splice(position, 1);
            }

            return false;
        }
        $(this).addClass('delitable');
        $(this).val($(this).data('id'));
        items.push($(this).data('id'));
        e.preventDefault();
    });
    $(".items_delete").on("click", function(e) {
        if (items.length == 0) {
            alert("Некорректный ввод");
        }
        $.ajax({
            url: "/material/delete",
            method: "GET",
            data: {
                items: items,
            },
            dataType: "json",
            success: function(data) {
                window.location.reload(false);
            },
        });
        e.preventDefault();
    });
    $(".save").on("click", function(e) {
        data = {};
        var sku_check = $(".sku_check").val();
        var title_check = $(".title_check").val();
        var quantity_check = $(".quantity_check").val();
        var type_check = $(".type_check").val();
        var photo_check = $(".photo_check").val();
        data.sku = sku_check;
        data.title = title_check;
        data.quantity = quantity_check;
        data.type = type_check;
        data.photo = photo_check;
        console.log(data, "D");
        $.ajax({
            url: "/field/update",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(data) {
                console.log(data, 'success');
                window.location.reload(false);

            },
        });
        e.preventDefault();
    });
    $('dropdown-toggle').on('click', function() {
        $('.dropdown-toggle').dropdown();
    })

    function rerenderList(e) {
        console.log("RERENDER");
        var val = $(".form-control").val();
        input = $(".form-control");
        $.ajax({
            url: "/material/ajax",
            method: "GET",
            data: {
                val: val
            },
            dataType: "json",
            success: function(data) {
                console.log(data, "DATA RE");
                var elem = $("#body");
                elem.empty();
                var city_list = JSON.parse(
                    '<?php print_r(json_encode(Cities::getCityList())); ?>');
                $.each(data.result, function(index, value) {
                    var href = '<?php echo Url::to(['/material/ajax/'], true);?>';
                    '<?php echo Url::to(['/material/update/?id='], true);?>';
                    var city = city_list[parseInt(value.type, 10)];
                    elem.append('<tr>' +
                        '<td><a href="' + href + value.id + '">' + value.title +
                        '</a></td>' +
                        '<td class="text-nowrap text-end">' +
                        '</td>' +
                        '<td class="text-nowrap text-end">' +
                        '</td>' +
                        '<td class="text-nowrap text-end">' +
                        '</td>' +
                        '<td class="text-nowrap text-end">' +
                        '</td>' +
                        '</tr>');

                });
                rerenderPagination(data);
            },
        });
        e.preventDefault();
    };

    function rerenderPagination(data) {
        console.log("RERENDER PAGINATION");
        $("#my_input").off("change paste keyup", function(e) {
            rerenderList(e);
        });
        $("#my_input").off("click", function(e) {
            rerenderList(e);
        });
        $("#pagination-ajax").empty();
        $.each(data.paginated_search, function(index, value) {
            var pagination_href =
                '<?php echo Url::to(['/material/ajax/'], true);?>';
            $("#pagination-ajax").append('<' +
                'li class = "page-item" >' +
                '<a data-count= "' + value.page_count +
                '" class ="page-link pag" >' + value.page_count +
                '</a>' +
                '</li>');
        });
        $("#ajax").show();
        $("#hide").css("display", "none");
        console.log($(".pag"),
            "GVG");
        $(".pag").on('click', function(e) {
            console.log("HERE");
            var data = {
                page_count: $(this).data('count'),
                val: $(".form-control").val(),
            }
            console.log(data,
                "DATA");
            pagination(data);
        });


    }

    function pagination(data) {
        console.log("PG");
        input = $(".form-control");
        $.ajax({
            url: "/material/ajax",
            method: "GET",
            data: data,
            dataType: "json",
            success: function(data) {
                var elem = $("#body");
                elem.empty();
                console.log("SUCCESS PAG", data);
                $.each(data.result, function(index, value) {
                    var href = '<?php echo Url::to(['/material/ajax/'], true);?>';
                    elem.append('<tr>' +
                        '<td><a href="' + href + value.id + '">' + value.title +
                        '</a></td>' +
                        '<td class="text-nowrap text-end">' +
                        '</td>' +
                        '<td class="text-nowrap text-end">' +
                        '</td>' +
                        '<td class="text-nowrap text-end">' +
                        '</td>' +
                        '<td class="text-nowrap text-end">' +
                        '</td>' +
                        '</tr>');
                });
                rerenderPagination(data);
            },
        });
    };
});
</script>

<h1 class="my-md-5 my-4"><?= Html::encode($this->title)  ?></h1>
<a class="btn btn-primary mb-4" href="<?php echo Url::to(['/material/create'], true);?>" role="button">Добавить </a>
<div class="row">
    <div class="col-md-8">

        <div class="input-group mb-3">
            <input id="my_input" type="text" class="form-control" name="MaterialSearch[text]" placeholder=""
                aria-label="Example text with button addon" aria-describedby="button-addon1">
            <button class="btn btn-primary find" type="submit" id="button-addon1">Искать</button>
        </div>

    </div>
    <div class="col-md-8">
        <form class="form-horizontal mb-3" id="filter" action="" method="get" role="form">
            <input type="hidden" name="action" value="filterSearch">

            <select class="form-control" name="productType" id="productType">
                <option value="0">Выберите город</option>
                <?php
                    foreach($models as $key => $value){
                        echo '<option value="' . $key . '">' . $value . '</option>';
                    }
                    ?>
            </select>
        </form>
    </div>
</div>
<div class="table-responsive">
    <?php if(!empty($result) && !empty($pages)) :?>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Изображение</th>
                <th scope="col">Описание</th>
                <th scope="col">Название</th>
                <th scope="col">Текст</th>
                <th scope="col">Город</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <form class="parent" method="POST" action="<?php echo Url::to(['/material/delete'], true);?>">
            <?=
                Html::hiddenInput(
                Yii::$app->request->csrfParam,
                Yii::$app->request->csrfToken
                );
            ?>
            <tbody id="body">
                <?php foreach($result as $item):?>
                <tr>
                    <td>
                        <?php if ($item['photo_id'] != null) :?>

                        <img
                            src="<?php echo Url::to(['/uploads/thumbs/'.Material::getPhotoThumbs($item['photo_id']).''], true);?>">
                        <?php endif; ?>
                    </td>
                    <td>
                        <?= $item['description']  ?>
                    </td>
                    <td>
                        <a
                            href="<?php echo Url::to(['/material/update/?id=' . $item['id'] .''], true);?>"><?=  Html::encode($item['title'])  ?></a>
                    </td>
                    <td>
                        <?= $item['text']  ?>
                    </td>
                    <td><?php echo Cities::getCityName($item['city_id'])  ?></td>
                    <td class="text-nowrap text-end">
                        <a href="<?php echo Url::to(['/material/update/?id=' . $item['id'] .''], true);?>"
                            class="text-decoration-none me-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-pencil" viewBox="0 0 16 16">
                                <path
                                    d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z" />
                            </svg>
                        </a>
                        <?php if(!Material::userHasNews($item['id'])):?>
                        <a href="<?php echo Url::to(['/material/favorites/?id=' . $item['id'] .''], true);?>"
                            type="button" class="btn btn-danger">Избранное</a>
                        <?php endif; ?>

                        <input name='delete' data-id="<?= $item['id']  ?>" class="form-check-input items"
                            type="checkbox" value="">
                        <label class="form-check-label">
                            Удалить
                        </label>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            <input type="button" class="btn-check items_delete" id="btn-check-3" autocomplete="off">
            <label class="btn btn-primary" for="btn-check-3">Удалить </label>
        </form>
    </table>
    <?php if( count($pages)>1 && $favorit == null ) :?>
    <nav id="hide" aria-label="Page navigation example">
        <ul class="pagination">
            <?php foreach($pages as $item):?>
            <?php if( !empty($_GET) 
                && isset($_GET['city_id'])) 
            :?>
            <li class="page-item"><a data-count="<?= $item['page_count']  ?>" class="page-link"
                    href="<?php echo Url::to(['/material/index?page_count='.$item['page_count'].'&city_id='.$_GET['city_id'].''], true);?>">
                    <?= $item['page_count']?></a>
            </li>
            <?php endif; ?>
            <?php if( empty($_GET) 
                && !isset($_GET['city_id'])) 
            :?>

            <li class="page-item"><a data-count="<?= $item['page_count']  ?>" class="page-link"
                    href="<?php echo Url::to(['/material/index?page_count='.$item['page_count'].''], true);?>">
                    <?= $item['page_count']?></a>
            </li>
            <?php endif; ?>
            <?php if(!empty($_GET)
                && isset($_GET['page_count'])
                && !isset($_GET['city_id']))
            :?>

            <li class="page-item"><a data-count="<?= $item['page_count']  ?>" class="page-link"
                    href="<?php echo Url::to(['/material/index?page_count='.$item['page_count'].''], true);?>">
                    <?= $item['page_count']?></a>
            </li>
            <?php endif; ?>

            <?php endforeach; ?>
        </ul>
    </nav>
    <?php endif;?>
    <?php if( count($pages)>1 && $favorit !== null ) :?>
    <nav id="hide" aria-label="Page navigation example">
        <ul class="pagination">
            <?php foreach($pages as $item):?>
            <?php if(!isset($_GET['city_id'])) :?>
            <li class="page-item"><a data-count="<?= $item['page_count']  ?>" class="page-link"
                    href="<?php echo Url::to(['/site/index?page_count='.$item['page_count'].''], true);?>"><?= $item['page_count']?></a>
            </li>
            <?php endif; ?>
            <?php if(isset($_GET['city_id'])) :?>
            <li class="page-item"><a data-count="<?= $item['page_count']  ?>" class="page-link"
                    href="<?php echo Url::to(['/site/index?page_count='.$item['page_count'].'&city_id='.$_GET['city_id'].''], true);?>"><?= $item['page_count']?></a>
            </li>
            <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </nav>
    <?php endif;?>
    <?php endif  ?>
</div>
<div id="ajax">
    <nav aria-label="Page navigation example">
        <ul id="pagination-ajax" class="pagination">
        </ul>
    </nav>
</div>