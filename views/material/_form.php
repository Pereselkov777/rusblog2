<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Material;
use app\models\Cities;
use dosamigos\selectize\SelectizeDropDownList ;
use vova07\imperavi\Widget;


/* @var $this yii\web\View */
/* @var $model app\models\Material */
/* @var $form yii\widgets\ActiveForm */

if (isset($_GET['id'])) {
    $this->title = Yii::t('app', 'Редактировать');
} else {
    $this->title = Yii::t('app', 'Добавить новость');
}
if ( 
    isset($model['id']) && 
    !empty($model['id'])
)   {
    $city_name = Cities::getCityName($model->city_id);
    $photo = Material::getPhoto($model->photo_id);
    $news = Material::getNews($model['id'],$model->city_id, $model->title);
} else {
    $photo = null;
}
$city_list = Cities::getCityList();
?>

<h1 class="my-md-5 my-4"><?= Html::encode($this->title) ?></h1>
<div class="row">
    <div class="col-lg-5 col-md-8">
        <form class="parent">
            <div class="form-floating mb-3">
                <input value="<?= !empty($model['id']) ? $model['description'] : ''?>" name="description" type="text"
                    class="form-control sku" placeholder="Введите код товара" id="floatingName">
                <label for="floatingName">Описание</label>
                <div class="invalid-feedback sku_error">
                    Пожалуйста, заполните поле
                </div>
            </div>
            <div class="form-floating mb-3">
                <input value="<?= !empty($model['id']) ? $model['title'] : ''?>" name="title" type="text"
                    class="form-control title" placeholder="Напишите название" id="floatingName">
                <label for="floatingName"> Название</label>
                <div class="invalid-feedback title_error">
                    Пожалуйста, заполните поле
                </div>
            </div>
            <div class="form-floating mb-3">
                <textarea value="<?= !empty($model['id']) ? $model['text'] : ''?>" name="text" type="text"
                    class="form-control quantity" placeholder="Напишите название"
                    id="floatingName"><?= !empty($model['id']) ? $model['text'] : ''?> </textarea>
                <label for="floatingName"> Текст</label>
                <div class="invalid-feedback quantity_error">
                    Пожалуйста, заполните поле
                </div>
            </div>
            <div class="form-floating mb-3">
                <select name="type" class="form-select type" id="floatingSelectCategory">
                    <option value="<?= !empty($model['id']) ? $model['city_id'] : ''?>" selected>
                        <?= !empty($model['id']) ? $city_name : 'Выберите город'?>
                    </option>
                    <?php
                        if (!empty($city_list)) {
                        foreach ($city_list as $key => $val)  {
                    ?>
                    <option value="<?php echo $key;?>"><?php echo $val;?></option>
                    <?php
                        }
                      }
                    ?>
                </select>
                <label for="floatingSelectCategory">Город</label>
                <div class="invalid-feedback  type_error">
                    Пожалуйста, заполните поле
                </div>
            </div>


            <?php if (isset($_GET['id'])): ?>
            <button data-id="<?= $model['id']?>" id="update" class="btn btn-primary mb-3">Добавить</button>
            <?php endif; ?>
            <?php if (!isset($_GET['id'])): ?>
            <button data-id="<?= $model['id']?>" id="create" class="btn btn-primary mb-3">Добавить</button>
            <?php endif; ?>
        </form>

    </div>
    <div class="col-lg-5">
        <?php if(!empty($news)) :?>
        Похожие новости
        <?php endif?>
        <div class="table-responsive">
            <?php if(!empty($news)) :?>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Изображение</th>
                        <th scope="col">Описание</th>
                        <th scope="col">Название</th>
                        <th scope="col">Город</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <form class="parent" method="POST" action="<?php echo Url::to(['/material/delete'], true);?>">
                    <?=
                        Html::hiddenInput(
                        Yii::$app->request->csrfParam,
                        Yii::$app->request->csrfToken
                        );
                    ?>
                    <tbody id="body">
                        <?php foreach($news as $item):?>
                        <tr>
                            <td>
                                <? if($item['photo_id']) :?>
                                <img
                                    src="<?php echo Url::to(['/uploads/thumbs/'.Material::getPhotoThumbs($item['photo_id']).''], true);?>">
                                <? endif ?>
                            </td>
                            <td>
                                <?= $item['description']  ?>
                            </td>
                            <td>
                                <a
                                    href="<?php echo Url::to(['/material/update/?id=' . $item['id'] .''], true);?>"><?=  Html::encode($item['title'])  ?></a>
                            </td>
                            <td><?php echo Cities::getCityName($item['city_id'])  ?></td>
                            <td class="text-nowrap text-end">
                                <a href="<?php echo Url::to(['/material/update/?id=' . $item['id'] .''], true);?>"
                                    class="text-decoration-none me-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-pencil" viewBox="0 0 16 16">
                                        <path
                                            d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z" />
                                    </svg>
                                </a>
                                <?php if(!Material::userHasNews($item['id'])):?>
                                <a href="<?php echo Url::to(['/material/favorites/?id=' . $item['id'] .''], true);?>"
                                    type="button" class="btn btn-danger">Избранное</a>
                                <?php endif; ?>

                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>

                </form>
            </table>
            <?php endif  ?>

        </div>
    </div>
    <div class="col-lg-5 col-md-8">
        <form class="upload_file" method="POST" enctype="multipart/form-data" style="display:none;"
            action="<?php echo Url::to(['/material/upload'], true);?>">

            <input value="<?= !empty($model['id'])&& $photo ? $photo->id : ''?>" name="photo" type="file"
                class="form-control photo" placeholder="Напишите название" id="floatingName">
            <?php if ($photo): ?>
            <img src="<?php echo Url::to(['/uploads/thumbs/'.Material::getPhotoThumbs($photo->id).''], true);?>">
            <?php endif; ?>
            <div class="invalid-feedback file_error">
                Пожалуйста, заполните поле
            </div>
            <?php if (!$model['id']): ?>
            <input name="is_new_photo" type="hidden">
            <?php endif; ?>
            <input name="id" id="upload" value="<?= !empty($model['id']) ? $model['id'] : ''?>" type="hidden">
            <button style="margin-left:70%" class="btn btn-primary mt-2">Upload</button>
        </form>
    </div>

</div>
</div>
<?php
$script = <<< JS
    jQuery(document).ready(function () {
        var elem;
        var url;
        data = {

        };
        if($("#update").length){
                console.log($("#update"), "UPDATE");
                var elem = $("#update");
                var url = "/material/ajax2";
                data.id = elem.data("id");
                $(".upload_file").show();
                actionMaterial(elem,url,data);
                return;
        }
        if($("#create").length){
            console.log($("#create"), "THIS CASE");
                var elem = $("#create");
                var url = "/material/create";
                $(".upload_file").hide();
                actionMaterial(elem,url,data);
                return;
        }
        function actionMaterial(elem,url,data){
            elem.click(function(e) {
                $(".invalid-feedback").css("display","none");
                var p = elem.closest('.parent'); 
                console.log(p, 'P');
                var city = p.find('.type').val();
                var title = p.find('.title').val();
                var description = p.find('.sku').val(); 
                var text = p.find('.quantity').val();
                if (city== "Выберите город") {
                    city = '';
                } 
                data.city = city;
                data.title = title;
                data.description = description;
                data.text = text;
                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    dataType: "json",
                    success: function (data) {         
                        if (data.description || data.city_id || data.title || data.text) {
                            if (data.city_id) {
                                $('.type_error').css("display","block");
                            } 
                            if(data.description ){
                                $('.sku_error').css("display","block");
                            }
                            if(data.text){
                                $('.quantity_error').css("display","block");
                            } 
                            if(data.title){
                                $('.title_error').css("display","block");
                            } 
                            $('.alert-danger').css('display', 'block');
                            $('.alert-danger').scrollTop();
                            return false;              
                        }

                        if($("#update").length){
                            p.find('.type').val(city);
                            p.find('.sku').val(description);
                            p.find('.title').val(title);
                            p.find('.quantity').val(text);
                            $('.alert-success').css('display', 'block');
                            $('.alert-danger').css('display', 'none');
                            $('.alert-success').scrollTop();
                            return false;
                        }
                        if($("#create").length){
                            p.find('.type').val(city);
                            p.find('.sku').val(description);
                            p.find('.title').val(title);
                            p.find('.quantity').val(text);
                            $("#upload").val(data.id);
                            $('.alert-success').css('display', 'block');
                            $('.alert-danger').css('display', 'none');
                            $('.alert-success').scrollTop();
                            $(".upload_file").css('display', "block");
                            return false;
                        }  
                    },
                }); 
                e.preventDefault();
            }); 

        }
           
    });

JS;
$this->registerJs($script);
?>