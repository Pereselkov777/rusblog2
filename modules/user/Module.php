<?php

namespace app\modules\user;
use Yii;

/**
 * user module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\user\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        \Yii::configure($this, [
            'components' => [
                'user' => [
                    'class' => 'yii\web\User',
                    'identityClass' => 'yii\modules\user\models\MyUser',
                ],
            ]
        ]);  
    }
    /**
     * @return Module
     */
    /**
     * @return string
     */
    public static function getDb()
    {
        return Yii::$app->getDb();
    }
}