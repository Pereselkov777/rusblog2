<?php

namespace app\modules\user\controllers;

use yii\web\Controller;
use app\modules\user\models\LoginForm;
use app\modules\user\models\EmailConfirm;
use yii\web\BadRequestHttpException;
use Yii;
use AmoCRM\OAuth2\Client\Provider\AmoCRM;
use app\components\Farm;
/**
 * Default controller for the `user` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionEmailConfirm($token)
    {
        try {
            $model = new EmailConfirm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->confirmEmail()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('module', 'FLASH_EMAIL_CONFIRM_SUCCESS'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('module', 'FLASH_EMAIL_CONFIRM_ERROR'));
        }

        return $this->goHome();
    }
}