<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "field".
 *
 * @property int $id ID
 * @property int $description
 * @property int $title title
 * @property int $text text
 * @property int $city_id city_id
 * @property int $client_id
 * @property int $photo
 */
class Field extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'field';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'title', 'text'], 'string', 'max'=>255],
            [['client_id','photo','city_id'], 'integer', 'max'=>10],
            [['client_id'], 'required'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'description'),
            'title' => Yii::t('app', 'title'),
            'text' => Yii::t('app', 'text'),
            'city_id' => Yii::t('app', 'city_id'),
            'client_id' => Yii::t('app', 'Client ID'),
            'photo' => Yii::t('app', 'Фото'),
        ];
    }
}