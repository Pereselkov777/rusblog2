<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Material;
use Yii;

/**
 * MaterialSearch represents the model behind the search form of `app\models\Material`.
 */
class MaterialSearch extends Material
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'city_id'], 'integer'],
            [['title', 'description','text'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * 
     *
     * @param array $params
     *
     * @
     */
    public function asyncSearch($params)
    {  
        $limit = $_ENV['LIMIT'];
        if (isset($params['page_count'])) {
            $offset = $limit * ($params['page_count'] -1);
	        $page = $params['page_count'];
        } else {
            $offset = null;
	        $page = 1;
        }
        $result = [];
        $news3 = [];
        if (isset($params['favorit'])) {
            $id = $params['favorit'];
            $count =  Material::find()->leftJoin(
                'news_users', 'news_users.news_id = material.id')
                ->where(['like','news_users.user_id',$id])->count();
            $pages = $this->counter($count);
            $result = Material::find()->leftJoin(
                'news_users', 'news_users.news_id = material.id')
                ->where(['like','news_users.user_id',$id])->limit($limit)
                ->distinct()->asArray()->all();
            if ($offset) {
                $result = Material::find()->leftJoin(
                    'news_users', 'news_users.news_id = material.id')
                    ->where(['like','news_users.user_id',$id])
                    ->limit($limit)->offset($offset)->distinct()->asArray()->all();
            }
            if (isset($params['city_id'])) {
                $final_result = [];
                $news3 = Material::find()->leftJoin(
                    'news_users', 'news_users.news_id = material.id')
                    ->where(['like','news_users.user_id',$id])
                    ->distinct()->asArray()->all();
                if (!empty($news3)) { 
                    $arr_id = [];
                    foreach ($news3 as $sub) {   
                        if($sub['city_id'] ==$params['city_id']){
                            $final_result[] = $sub;
                        }
                    }
                }
                $limit_array = array_slice($final_result,0,$limit);
                $count = count($final_result);
                $pages = $this->counter($count);
                if ($offset) {
                    $news3 = Material::find()->leftJoin(
                        'news_users', 'news_users.news_id = material.id')
                        ->where(['like','news_users.user_id',$id])
                        ->distinct()->asArray()->all();
                    $final_result = [];
                    if (!empty($news3)) { 
                        $arr_id = [];
                        foreach ($news3 as $sub) {   
                            if($sub['city_id'] ==$params['city_id']){
                                $final_result[] = $sub;
                            }
                        }
                    }
                    $offset_array = array_slice($final_result,$offset,$limit);            
                    $results = [
                        'pages'=>$pages,
                        'result'=>$offset_array,
                        ];
                    return $results;
                }
                $results = [
                    'pages'=>$pages,
                    'result'=>$limit_array,
                    ];
                return $results;
                
            }   else {
                $results = [
                    'pages'=>$pages,
                    'result'=>$result,
                    ];
                return $results;
            }    
        }
        $news1 = [];
        if (isset($params['city_id'])) {
            $city_id = $params['city_id'];	    
	        $count =  Material::find()->where(['city_id'=>$city_id])->count();
            $pages = $this->counter($count,$page);
            if ($offset) {
                $news1 = Material::find()->where(['city_id'=>$city_id])
                ->limit($limit)->offset($offset)->asArray()->all();
                $results = [
                'pages'=>$pages,
                'result'=>$news1,
                ];
                return $results;
            }
            $news1 = Material::find()->where(['city_id'=>$city_id])
            ->limit($limit)->asArray()->all();
            if (empty($news1)) {
                return $news1;
            } else {
                foreach($news1 as $sub){
                    $result[] = $sub;
                }
                $results = [
                	'pages'=>$pages,
 			        'result'=>$result,
                ];
		        return $results;
            }
        }
        if (!isset($params['val'])) {
            if (!isset($params['city_id'])) {
                $result =  Material::find()
                ->limit($limit)->asArray()->all();
	         $count =  Material::find()->count();
                 $pages = $this->counter($count,$page);
                if ($offset) {
                    $result = Material::find()
                    ->limit($limit)->offset($offset)->asArray()->all();
                }
                $results = [
                    'pages' =>$pages,
                    'result'=> $result,
                ];
                return $results;
            }
            $filter_result = [];
            foreach ($result as $sub) { 
                if ($sub['city_id'] == $params['city_id'] ) {
                    $filter_result[]=$sub;
                }
            }
            return $filter_result;
        }
        if (isset($params['val'])) {
            if (!isset($params['city_id'])) {
                $match = $params['val'];
                $val = "%".$match."%";
                $count =  Material::find()
                ->orFilterWhere(['like','material.title',$match])
                ->orFilterWhere(['like', 'material.description', $match])
                ->orFilterWhere(['like', 'material.text', $match])->count();
                $pages = $this->counter($count);
                if ($offset) {
                    
                    $result = Material::find()
                    ->orFilterWhere(['like','material.title',$match])
                    ->orFilterWhere(['like', 'material.description', $match])
                    ->orFilterWhere(['like', 'material.text', $match])
                    ->limit($limit)->offset($offset)->asArray()->all();
                    $results = [
                        'val' => $match,
                        'pages' =>$pages,
                        'result'=> $result,  
                    ];
		            return $results;
                }
                $result = Material::find()
                ->orFilterWhere(['like','material.title',$match])
                ->orFilterWhere(['like', 'material.description', $match])
                ->orFilterWhere(['like', 'material.text', $match])
                ->limit($limit)->distinct()->asArray()->all();
                $results = [
                    'val' => $match,
                    'pages'=>$pages,
                    'result'=>$result,
                ];
                return $results;   
            }
            if ($offset) { 
                $result = Material::find()->where(['city_id'=>$params['city_id']])
                ->orFilterWhere(['like','material.title',$match])
                ->orFilterWhere(['like', 'material.description', $match])
                ->orFilterWhere(['like', 'material.text', $match])
                ->limit($limit)->offset($offset)->asArray()->all();
            }
            $result = Material::find()->where(['city_id'=>$params['city_id']])
            ->orFilterWhere(['like','material.title',$match])
            ->orFilterWhere(['like', 'material.description', $match])
            ->orFilterWhere(['like', 'material.text', $match])
            ->limit($limit)->distinct()->asArray()->all(); 
            return $result;
        }
        return $result;    
    }
    public function counter($count)
    {
        $limit = 6;
        if ($count == 0) {
        $count = Material::find()->count();
        }
        if($count != 0) {
            $page_number = $count / $limit;
            if (is_double($page_number)) {
                $page_number = (int) $page_number + 1;
            }
            $pages = [];
            for ($i=1;$i<=$page_number;$i++) {
                $pages[] = [
                'limit' =>$limit,
                'page_number'=>$page_number,
                'page_count' => $i,
                ];
            }
            return $pages;
        } else {
            return null;
        }	    
    }    
}