<?php

namespace app\models;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Photo;
use yii\db\Expression;
use app\models\Fields;
use Yii;

/**
 * This is the model class for table "material".
 *
 * @property int $id ID РјР°С‚РµСЂРёР°Р»Р°
 * @property int $type РўРёРї
 * @property int $text
 * @property int $description
 * @property int $photo_id
 * @property string $title РќР°Р·РІР°РЅРёРµ
 */
class Material extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text',], 'string'],
            [['photo_id','city_id'], 'integer'],
            [['text','description','title','city_id'], 'required'],
            [['description','title'], 'string', 'max' => 255],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID РјР°С‚РµСЂРёР°Р»Р°'),
            'type' => Yii::t('app', 'РўРёРї'),
            'title' => Yii::t('app', 'РќР°Р·РІР°РЅРёРµ'),
            'photo_id' => Yii::t('app', 'РР·РѕР±СЂР°Р¶РµРЅРёРµ'),
            'sku' => Yii::t('app', 'SKU'),
            'quantity' => Yii::t('app', 'РљРѕР»-РІРѕ РЅР° СЃРєР»Р°РґРµ'),
        ];
    }

    public static function getPhoto($id)
    {
       $photo = Photo::findOne($id);
        return $photo;
    }
    public static function getPhotoThumbs($id)
    {
        $photo = Photo::findOne($id);
        if ($photo) {
            $name = $photo->thumbs;
            return $name;
        } else {
            return null;
        } 
     }
     public static function userHasNews($id)
     {
        $result = self::find()->leftJoin(
            'news_users', 'news_users.news_id = material.id')
            ->where(['news_users.user_id' => Yii::$app->user->id,'news_users.news_id'=>intval($id)])->distinct()->asArray()->all();
        if ($result) {
            return $result;

        } else {
            return null;
        }  
     }
     public static function getNews($id, $city_id, $title)
    { 
        $match = explode(' ',trim($title))[0];
        $match2 = substr($title, 0,4); 
        $titles = [];
        $matches = explode(' ',trim($title));
        $materials = self::find()->where(['<>','id', intval($id)])->asArray()->all();
        $words = [];
        foreach ($materials as $sub) {
            $words[$sub['id']] = explode(' ',trim($sub['title']));
        }
	    $result = [];
        $materials = [];
        foreach ($words as $sub) {
            foreach ($sub as $s) {
                if (in_array ($s, $matches)) {
                     $materials = self::find()
                    ->where(['id'=> array_search($sub, $words)])
                    ->asArray()->all();
                }
            } 
        }
        foreach ($materials as $sub) {
            $result[] = $sub;
        }
        $query =self::find()->where(['like', 'title', $match])
        ->orfilterWhere(['material.city_id'=>$city_id])
        ->orfilterWhere(['like','material.description',$match])
        ->orfilterWhere(['like','material.title',$match2])
        ->andWhere(['<>','id', intval($id)])->asArray()->limit(10)->all();
        $arr_id = [];
        if (!empty($result)) {
            foreach ($result as $sub) {
                $arr_id[] = $sub['id'];
                
            }
        }
        if (!empty($query)) { 
            foreach ($query as $sub) { 
                if(!in_array($sub['id'],$arr_id)){ 
                    $result[]= $sub;
                }
            } 
        }
        if (!empty($result)) {
            return $result;
        } else {
            return null;
        }       
     }
    public static function createDefaultFields($user_id)
    {
        $fields = new Field();
        $fields->client_id = $user_id;
        if (!$fields->save()) {
            print_r(json_encode($fields->getErrors()));
            exit();
        }
    }
    public static function getFields($user_id)
    {
        $fields = Field::find()->where(["client_id"=>$user_id])->one();
        return $fields;
    }   
}