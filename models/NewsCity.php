<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_city".
 *
 * @property int $news_id
 * @property int $city_id
 */
class NewsCity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id', 'city_id'], 'required'],
            [['news_id', 'city_id'], 'integer'],
            [['news_id', 'city_id'], 'unique', 'targetAttribute' => ['news_id', 'city_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'news_id' => Yii::t('app', 'News ID'),
            'city_id' => Yii::t('app', 'City ID'),
        ];
    }
}
