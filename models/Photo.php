<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "photo".
 *
 * @property int $id ID изображения
 * @property string $name Название
 * @property string $full full
 * @property string $thumbs thumbs
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'full', 'thumbs'], 'required'],
            [['name'], 'string', 'max' => 20],
            [['full', 'thumbs'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID изображения'),
            'name' => Yii::t('app', 'Название'),
            'full' => Yii::t('app', 'full'),
            'thumbs' => Yii::t('app', 'thumbs'),
        ];
    }
}
