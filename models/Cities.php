<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use Yii;

/**
 * This is the model class for table "cities
 *".
 *
 * @property int $id ID 
 * @property string $name Название 
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
        ];
    }


    public static function getCityList()
    {
        $city_models = parent::find()->all();
        $city_list = ArrayHelper::map($city_models,'id','name');
        return $city_list;
    }


    public static function getCityName($id)
    {
      if ($id) {
        $list = self::getCityList();
        $name = $list[$id];
      return Html::encode($name);

      } else {
        return null;
      }
      
    }
}