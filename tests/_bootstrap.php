<?php

//echo('its me'); 
define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);

require_once __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require __DIR__ .'/../vendor/autoload.php';
$config = require __DIR__ .'/../config/test.php';

new yii\web\Application($config);

Yii::$classMap['MyClass'] = require __DIR__ .'/../components/MyClass.php';
Yii::$classMap['SessionHelper'] = require __DIR__ .'/../components/SessionHelper.php';