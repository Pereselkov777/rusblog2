<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%my_user}}`.
 */
class m210613_164754_add_role_column_to_my_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%my_user}}', 'role', $this->smallInteger()->after('status')->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%my_user}}', 'role');
    }
}